% Kinematics simulation of bio-bat

clc;
clear all;
close all;
% q is the motor angle
params;

temp1 = l1_R/l3_R;
theta = asin(temp1);

n = 50;
q1 = linspace(0,2*pi,n);


for i = 1:n
[pa_R(:,i),pb_R(:,i),pc_R(:,i),pd_frame_R(:,i),pd_R(:,i),pe_R(:,i),pf_R(:,i)...
 pa_L(:,i),pb_L(:,i),pc_L(:,i),pd_frame_L(:,i),pd_L(:,i),pe_L(:,i),pf_L(:,i)   ] = func_fwdGeneral(q1(i));
end


% 



figure;
filename = 'sim_bat.gif';
for i = 1:n
    
% plot 3d of the robot
% right side
% plot([pa_R(1,i),pb_R(1,i)],[pa_R(2,i),pb_R(2,i)]);

plot([pa_R(1,i),pa_R(1,i)],[pa_R(2,i),pa_R(2,i)],'o');
hold on
plot([pd_R(1,i),pd_R(1,i)],[pd_R(2,i),pd_R(2,i)],'o');
plot([pb_R(1,i),pb_R(1,i)],[pb_R(2,i),pb_R(2,i)],'o');

plot([pc_R(1,i),pb_R(1,i)],[pc_R(2,i),pb_R(2,i)]);
plot([pc_R(1,i),pe_R(1,i)],[pc_R(2,i),pe_R(2,i)]);
plot([pe_R(1,i),pf_R(1,i)],[pe_R(2,i),pf_R(2,i)]);
% left side
% plot([pa_L(1,i),pb_L(1,i)],[pa_L(2,i),pb_L(2,i)]);
plot([pc_L(1,i),pb_L(1,i)],[pc_L(2,i),pb_L(2,i)]);
plot([pc_L(1,i),pe_L(1,i)],[pc_L(2,i),pe_L(2,i)]);
plot([pe_L(1,i),pf_L(1,i)],[pe_L(2,i),pf_L(2,i)]);

cam = plot([pa_R(1,i),pd_R(1,i)],[pa_R(2,i),pd_R(2,i)],'linewidth',2);

% plot([pe(1,i),pd(1,i)],[pe(2,i),pd(2,i)]);

hold off
legend([cam],'cam');
axis equal
xlim([-1.8 1.8]);
ylim([-1.8 1.8]);
title('bio-bat');
xlabel('x');
ylabel('y');



box on


% set the window size
% set(gcf,'position',[100,60,1300,870]);

% pause(0.2);
       frame = getframe(gcf);
    im = frame2im(frame);
    [imgin, cm] = rgb2ind(im, 256);
    
    if i == 1
        imwrite(imgin,cm,filename,'gif', 'Loopcount', inf);
    else
        imwrite(imgin,cm,filename,'gif', 'WriteMode','append');
    end
    

end




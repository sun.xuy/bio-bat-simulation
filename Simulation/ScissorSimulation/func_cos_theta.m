
% obtain cos(theta) based on given params a, b, c dims
%
% input:
%   a: dim of line in front of theta
%   b, c: dim of lines adjacent to theta
%
% outout:
%   cos(theta)
function q = func_cos_theta(a,b,c)
q = (a^2-b^2-c^2)/(-2*b*c);
end
% simulation of triple scissor simulations
% Written by Xuyang Sun, 2019/9/26, Boston, MA

close all;
clear all;
clc;

% set up params
n = 20;
a = 0.1;
% set up variables
l1 = linspace(1.2*a,0.6*a,n);
l2 = linspace(a,1.1*a,n);
l3 = linspace(a,1.1*a,n);

for i = 1:n
%     solve for trigonometry
[q1(:,i),q2(:,i),q3(:,i),q4(:,i),q5(:,i),q6(:,i),q7(:,i),q8(:,i)] = func_trigonometry(l1(i),l2(i),l3(i),a);
%     calculate the position
[p1(:,i),p2(:,i),p3(:,i),p4(:,i),p5(:,i),p6(:,i),p7(:,i),p8(:,i),p9(:,i),p10(:,i),p11(:,i)] = func_fwd_kinematics(q1(i),q2(i),q3(i),q4(i),q5(i),q6(i),q7(i),q8(i),a,l1(i),l2(i),l3(i));
end

%%
% plot the animation
scrsz = get(groot,'ScreenSize');
fh = figure('Name','scissor simulation',...
    'Renderer','opengl',...
    'GraphicsSmoothing','on');
ah = axes('Box','on',...
    'XGrid','off',...
    'YGrid','off',...
    'DataAspectRatio',[1,1,1],...
    'PlotBoxAspectRatio',[1,1,1],...
    'Parent',fh);
xlabel(ah,'[m]');
ylabel(ah,'[m]');
xlim(ah,[-0.1 0.7]);
ylim(ah,[-0.25 0.15]);
hold(ah,'off');
title('Scissor Simulation');

filename = 'sim_mechanism.gif';
for i = 1:n
    cla(ah);   
    line([p1(1,i),p2(1,i)],[p1(2,i),p2(2,i)]);
    hold on
    line([p1(1,i),p9(1,i)],[p1(2,i),p9(2,i)]);
    line([p9(1,i),p2(1,i)],[p9(2,i),p2(2,i)]);
    line([p9(1,i),p3(1,i)],[p9(2,i),p3(2,i)]);
    line([p9(1,i),p4(1,i)],[p9(2,i),p4(2,i)]);
    line([p10(1,i),p3(1,i)],[p10(2,i),p3(2,i)]);
    line([p10(1,i),p4(1,i)],[p10(2,i),p4(2,i)]);
    line([p10(1,i),p5(1,i)],[p10(2,i),p5(2,i)]);
    line([p10(1,i),p6(1,i)],[p10(2,i),p6(2,i)]);
    line([p11(1,i),p5(1,i)],[p11(2,i),p5(2,i)]);
    line([p11(1,i),p6(1,i)],[p11(2,i),p6(2,i)]);
    line([p11(1,i),p7(1,i)],[p11(2,i),p7(2,i)]);
    line([p11(1,i),p8(1,i)],[p11(2,i),p8(2,i)]);
    
    line(p8(1,i),p8(2,i),'Marker','o','Color','r');
    line(p7(1,i),p7(2,i),'Marker','o','Color','k');
    ah3 = line(p2(1,i),p2(2,i),'Marker','o','Color','b');
    
    ah1 = plot(p8(1,1:i),p8(2,1:i),'Color','r');
    ah2 = plot(p7(1,1:i),p7(2,1:i),'Color','k');
    legend([ah3,ah1,ah2],'Actuation Point','Point1','Point2')
       
    
    
    frame = getframe(gcf);
    im = frame2im(frame);
    [imgin, cm] = rgb2ind(im, 256);
    
    if i == 1
        imwrite(imgin,cm,filename,'gif', 'Loopcount', inf);
    else
        imwrite(imgin,cm,filename,'gif', 'WriteMode','append');
    end
    
    
    
end






function [q1,q2,q3,q4,q5,q6,q7,q8] = func_trigonometry(l1,l2,l3,a)

% q_abh is the angle between ab and bh

% first triangle
[q_abh,~,q2] = func_triangle_angles(a,a,l1);
q1 = pi/2-q_abh;

% l_cd is the length between c and d
l_cd = sqrt(l2^2+a^2-2*a*l2*cos(q2));

% second triangle
[q_hdc,q_hcd,~] = func_triangle_angles(l2,a,l_cd);

% third triangle
[q5,q_dci,q_cdi] = func_triangle_angles(l_cd,a,a);
q3 = pi-q_hcd-q_dci;
q4 = pi-q_hdc-q_cdi;

% fourth triagle
l_ef = sqrt(l3^2+a^2-2*a*l3*cos(q5));
[q_efi,q_ief,~] = func_triangle_angles(l3,a,l_ef);

% fifth triangle
[q_efj,q_fej,q8] = func_triangle_angles(a,a,l_ef);
q6 = pi - q_ief-q_fej;
q7 = pi - q_efi-q_efj;




end
%   use trigonometry to obtain the angles of a triangle with
%   input:
%   lines with dims a,b and c

% output:
%   theta1: angle between b and c
%   theta2: angle between a and c
%   theta3: angle between a and b
function [theta1,theta2,theta3] = func_triangle_angles(a,b,c)

c1 = func_cos_theta(a,b,c);
c2 = func_cos_theta(b,c,a);
c3 = func_cos_theta(c,a,b);

theta1 = acos(c1);
theta2 = acos(c2);
theta3 = acos(c3);

end

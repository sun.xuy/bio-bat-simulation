% use trigonometry to obtain the angles of a triangle 
%
% input:
%   lines with dims a,b and c
%
% output:
%   theta1: angle between a and c
%   theta2: angle between c and b
%   theta3: angle between b and a
%
% By Xuyang Sun
function [theta1,theta2,theta3] = func_triangle_angles(a,b,c)
c1 = func_law_of_cosine(a,b,c);
c2 = func_law_of_cosine(b,a,c);
c3 = func_law_of_cosine(c,b,a);

theta1 = acos(c1);
theta2 = acos(c2);
theta3 = acos(c3);
end

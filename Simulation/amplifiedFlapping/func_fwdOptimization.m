function [pa_R,pb_R,pc_R,pd_frame_R,pd_R,pe_R,pf_R,pg_R,q2] = func_fwdOptimization(x,q1)



params;


% define transformation matrix

Tx = @(x,y,z,q) [1      0        0    x;...
                 0    cos(q)   sin(q) y;...
                 0    -sin(q)  cos(q) z;...
                 0      0        0    1];

Ty = @(x,y,z,q) [cos(q)   0   sin(q)  x;...
                    0    1       0   y ;...
               -sin(q)   0   cos(q)  z ;...
                    0    0       0   1 ];
                
Tz = @(x,y,z,q) [cos(q)   -sin(q)   0   x;...
                 sin(q)    cos(q)   0   y ;...
                    0       0       1   z ;...
                    0       0       0   1 ];


% calculate all the angles in the bat model
%%

q1 = mod(q1,2*pi);
temp1 = x(1)/x(6);
theta1 = asin(temp1);
% cam does not touch the bio-bat frame for only one cam bar
if q1 < theta1 || q1 > pi-theta1
% kinematics of right side of the robot   

% angles for right half of the bat
alpha1_R = q1;

% cam
Tab_R = Tz(0,x(5),0,-(pi-alpha1_R));
Tbd_R = Tz(0,x(6),0,0);

% from a to e
Tac_R = Tz(x(1),0,0,0);
Tce_R = Tz(0,x(2),0,0);


Pe_R = Tac_R*Tce_R;
pe_R = Pe_R(1:3,4);


% vector ef
pf_R = [x(7);x(8);0];
V_ef = pf_R-pe_R;
alpha7_R = atan2(V_ef(2),V_ef(1));
l3_R = sqrt(V_ef.'*V_ef);
q2 = alpha7_R;
T_framee_pr_R = Tz(0,0,0,q2);
Te_prf_R = Tz(l3_R,0,0,0);
Tfg_R = Tz(x(4),0,0,0);


Pd_R = Tab_R*Tbd_R;
Pc_R = Tac_R;
Pb_R = Tab_R;
Pg_R = Tac_R*Tce_R*T_framee_pr_R*Te_prf_R*Tfg_R;
Pf_R = Tac_R*Tce_R*T_framee_pr_R*Te_prf_R;

pg_R = Pg_R(1:3,4);
pf_R = Pf_R(1:3,4);
pd_R = Pd_R(1:3,4);
pd_frame_R = [0;0;0];
pc_R = Pc_R(1:3,4);
pb_R = Pb_R(1:3,4);
pa_R = [0;0;0];


elseif q1 >= theta1 && q1<= pi-theta1 
% kinematics of right side of the robot 

% angles for right half of the bat
alpha1_R = q1;
lbd_R = sqrt(x(6)^2+x(5)^2-2*x(6)*x(5)*cos(alpha1_R));


% triangle ADB, 3rd is q1
[alpha2_R,alpha3_R,~] = func_triangle_angles(x(6),x(5),lbd_R);

% triangle ACD
l2a_R = sqrt(lbd_R^2-x(1)^2);
l2b_R = x(2)-l2a_R;
[alpha4_R,alpha5_R,~] = func_triangle_angles(l2a_R,x(1),lbd_R);

% cam
Tab_R = Tz(0,x(5),0,-(pi-alpha1_R));
Tbd_R = Tz(0,x(6),0,0);

% from a to e
Taa_pr_R = Tz(0,0,0,-(alpha2_R+alpha4_R));
Ta_pac_R = Tz(0,x(1),0,pi/2);
Tcd_frame_R = Tz(0,l2a_R,0,0);
Td_framee_R = Tz(0,l2b_R,0,0);

Pe_R = Taa_pr_R*Ta_pac_R*Tcd_frame_R*Td_framee_R;
pe_R = Pe_R(1:3,4);


% vector ef
pf_R = [x(7);x(8);0];
V_ef = pf_R-pe_R;
alpha7_R = atan2(V_ef(2),V_ef(1));
alpha6_R = pi-alpha2_R-alpha4_R;
l3_R = sqrt(V_ef.'*V_ef);
q2 = alpha7_R;

T_framee_pr_R = Tz(0,0,0,-(alpha6_R-alpha7_R));
Te_prf_R = Tz(0,l3_R,0,0);
Tfg_R = Tz(0,x(4),0,0);

Pd_frame_R = Taa_pr_R*Ta_pac_R*Tcd_frame_R;
Pd_R = Tab_R*Tbd_R;
Pc_R = Taa_pr_R*Ta_pac_R;
Pb_R = Tab_R;
Pg_R = Taa_pr_R*Ta_pac_R*Tcd_frame_R*Td_framee_R*T_framee_pr_R*Te_prf_R*Tfg_R;
Pf_R = Taa_pr_R*Ta_pac_R*Tcd_frame_R*Td_framee_R*T_framee_pr_R*Te_prf_R;

pg_R = Pg_R(1:3,4);
pf_R = Pf_R(1:3,4);
pd_R = Pd_R(1:3,4);
pd_frame_R = Pd_frame_R(1:3,4);
pc_R = Pc_R(1:3,4);
pb_R = Pb_R(1:3,4);
pa_R = [0,0,0];





end

end














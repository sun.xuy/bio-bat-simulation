function [x,J] = func_getParam(x0,q_d,q1,n)


% set lower bound and upper bound
A = [];
b = [];
Aeq = [];
beq = [];
lb = [  0.1 0.3   0  0.4  0.2  0.15 0.2 0.4];
ub = [  1     2   0  3     2    2.5  3   5 ];



options = optimset('Display','off');

 [x,J] = fmincon(@func_cost,x0,A,b,Aeq,beq,lb,ub);
 
 
 
%  function [c,ceq] = loopconstraint(x)
%         c =[] ;
%         ceq =[] ;
%  
%  end

 
 function J = func_cost(x)
 
    for i = 1:n
   
        [~,~,~,~,~,~,~,~,q2(i)] = func_fwdOptimization(x,q1(i));
    
    end
    
    J = (q_d-q2)*(q_d-q2).';
 
 end





 end






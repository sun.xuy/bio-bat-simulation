
clc;
clear all;
% x0 = [A B C  delta_x delta_y ]
x0 = [2 3 4  ...
    3.4 1];
n = 20;
theta = linspace(20,-20,n)*pi/180;
q2_d = linspace(-5,-30,n)*pi/180;



% runing fmincon to obtain optimal params for the flapping mechanism
[x,J] = func_getParam(x0,q2_d,n,theta);





function [x,J] = func_getParam(x0,q2_d,n,theta)

% set up linear constraints
A = [];
b = [];
Aeq = [];
beq = [];
% set lower bound and upper bound
lb = [0.5 0.5 0.5  0.1 -5];
ub = [5 5 5  5 5];



options = optimset('Display','off');
% run fmincon
[x,J] = fmincon(@(x) func_cost(x,n,theta,q2_d),x0,A,b,Aeq,beq,lb,ub,@(x)loopConstraint(x,n,theta,q2_d),options);




    function J = func_cost(x,n,theta,q2_d)

        
        for i = 1:n
            [q2(1,i)] = func_FWDoptimization(theta(i),x);
        end
        J = (q2_d-q2)*(q2_d-q2).';
        
    end



    function [c,ceq] = loopConstraint(x,n,theta,~)

        for i = 1:n
            [q2(1,i),p2(:,i),p3(:,i)] = func_FWDoptimization2(theta(i),x);
        end
        
        
        
        c = [];
        %         c = [p3(1,:)-p2(1,:)];
        a1 = double(imag(q2)~=0);
        ceq = a1;
        
    end




end
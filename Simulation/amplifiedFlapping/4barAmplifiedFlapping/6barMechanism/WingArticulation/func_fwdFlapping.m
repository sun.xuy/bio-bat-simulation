
function  [pA,pE,p1,p2,p3,p4,...
    pb_R,pd_R,pc_R,pe_R,...
    pE_L,p1_L,p2_L,p3_L,p4_L,...
    pb_L,pd_L,pc_L,pe_L,q2,q2_L] = func_fwdFlapping(q1,alpha_rotation)

% p1_temp,p2_temp,p3_temp,p4_temp,q3,p_wingTip
% import the params
params;

% the wing stands still and only the cam is rotating
if q1<alpha_rotation || q1>pi-alpha_rotation
    alpha1 = alpha_rotation;
% the wing is flapping
elseif q1<=pi-alpha_rotation && q1>=alpha_rotation
    alpha1 = q1;
end
%% expand the wing
% right side of the robot
% length of link a and d

a = sqrt(l1^2+l2^2);
d = sqrt(x1^2+y1^2);
D = sqrt((y2-y1)^2+(x2-x1)^2);
alpha8 = atan2(y2-y1,x2-x1);
alpha9 = atan2(y1,x1);

% please refer to notes
% angles and length from the bat frame
% lad is the length from point A to D
% l2a is the length from point C to D
lad = sqrt(lh^2+lr^2-2*lh*lr*cos(alpha1));
l2a = sqrt(lad^2-l1^2);

alpha2 = acos((lad^2+lh^2-lr^2)/(2*lad*lh));
alpha4 = acos((lad^2+l1^2-l2a^2)/(2*lad*l1));
alpha6 = pi-alpha2-alpha4;

% position of point E
pex1 = l1*sin(alpha6)+l2*cos(alpha6);
pey1 = -l1*cos(alpha6)+l2*sin(alpha6);
% pE = [pex1 pey1 0]';

% find theta1, which is a function of input q1
theta1 = acos(pex1/a)-alpha9;


% first 4 bar mechanism
% please refer to notes
% e is the length from point E to point 1
e = sqrt(a^2 + d^2 - 2*a*d*cos(theta1));

alfa1 = asin(a*sin(theta1)/e);
beta1 = acos((e^2 + A^2 - b^2)/(2*A*e));

% find theta2, which is a function of theta1, meaning that theta2 is also a
% function of input q1
theta2 = pi-alpha8-alfa1-beta1+alpha9;

% second 4 bar mechanism
% please refer to notes
% l_E is the length from point 2 to point 4
l_E = sqrt(A^2 + D^2 - 2*A*D*cos(theta2));
alfa2 = asin(A*sin(theta2)/l_E);
beta2 = acos((l_E^2 + C^2 - B^2)/(2*l_E*C));


% calculate the position of each point in bat frame
% point E,B,D,C on the bat frame
pe_R = [pex1 pey1 0]';
pb_R = [0 lh 0]';
pd_R = pb_R + [lr*sin(q1) -lr*cos(q1) 0].';
pc_R = [l1*sin(alpha6) -l1*cos(alpha6) 0]';

% calculate the position of each point in first four bar mechanism
pE = [pex1 pey1 0]';
pA = [0 0 0].';
p1 = [x1 y1 0].';
p1_pr = [d*cos(alpha9) d*sin(alpha9) 0].';
p2 = [x1+A*cos(theta2+alpha8) y1+A*sin(theta2+alpha8) 0].';


% calculate the position of each point in second four bar mechanism
% p1 = [x1 y1 0].';
% p2 = [x1+A*cos(theta2+alpha8) y1+A*sin(theta2+alpha8) 0].';
p4 = [x2 y2 0].';
p3 = p4+ [-C*cos(alfa2+beta2-alpha8),C*sin(alfa2+beta2-alpha8),0].';
% p4_pr = p1+([D*cos(alpha8),D*sin(alpha8),0]).';
% p4_prpr = [d*cos(alpha9)+D*cos(alpha8) d*sin(alpha9)+D*sin(alpha8) 0].';


% position of the wing
% p_wing = p4+[-W*cos(alfa2+beta2-alpha8),W*sin(alfa2+beta2-alpha8),0].';
p_wing = [];
p_wing_L = [];
% output q2, which is the angle from horizontal to link34                
q2 = pi-(alfa2+beta2-alpha8)
                


%% left side of the robot
% a = sqrt(l1^2+l2^2);
% d = sqrt(x1^2+y1^2);
% D = sqrt((y2-y1)^2+(x2-x1)^2);
% alpha8 = atan2(y2-y1,x2-x1);
% alpha9 = atan2(y1,x1);

% please refer to notes
% angles and length from the bat frame
% lad is the length from point A to D
% l2a is the length from point C to D
alpha1_l = pi-alpha1;
lad_l = sqrt(lh^2+lr^2-2*lh*lr*cos(alpha1_l));
l2a_l = sqrt(lad_l^2-l1^2);
alpha2_l = acos((lad_l^2+lh^2-lr^2)/(2*lad_l*lh));
alpha4_l = acos((lad_l^2+l1^2-l2a_l^2)/(2*lad_l*l1));
alpha6_l = pi-alpha2_l-alpha4_l;

% position of point E
pex1_l = -(l1*sin(alpha6_l)+l2*cos(alpha6_l));
pey1_l = -l1*cos(alpha6_l)+l2*sin(alpha6_l);
% pE_L = [pex1_l pey1_l 0]';

% find theta1, which is a function of input q1
theta1_l = acos(abs(pex1_l)/a)-alpha9;



% first 4 bar mechanism
% please refer to notes
% e is the length from point E to point 1
e_l = sqrt(a^2 + d^2 - 2*a*d*cos(theta1_l));
alfa1_l = asin(a*sin(theta1_l)/e_l);
beta1_l = acos((e_l^2 + A^2 - b^2)/(2*A*e_l));


theta2_l = pi-alpha8-alfa1_l-beta1_l+alpha9;



% second 4 bar mechanism
% please refer to notes
% l_E is the length from point 2 to point 4
l_E_l = sqrt(A^2 + D^2 - 2*A*D*cos(theta2_l));
alfa2_l = asin(A*sin(theta2_l)/l_E_l);
beta2_l = acos((l_E_l^2 + C^2 - B^2)/(2*l_E_l*C));


% calculate the position of each point in bat frame
% point E,B,D,C on the bat frame
pe_L = [pex1_l pey1_l 0]';
pb_L = [0 lh 0]';
pd_L = pb_L + [-lr*sin(q1) lr*cos(q1) 0].';
pc_L = [-l1*sin(alpha6_l) -l1*cos(alpha6_l) 0]';

% calculate the position of each point in first four bar mechanism
pE_L = [pex1_l pey1_l 0]';
pA_L = [0 0 0].';
p1_L = [x1_l y1_l 0].';
% p1_pr_L = [-d*cos(alpha9) d*sin(alpha9) 0].';
p2_L = [x1_l-A*cos(theta2_l+alpha8) y1_l+A*sin(theta2_l+alpha8) 0].';


% calculate the position of each point in second four bar mechanism
% p1 = [x1 y1 0].';
% p2 = [x1+A*cos(theta2+alpha8) y1+A*sin(theta2+alpha8) 0].';
p4_L = [x2_l y2_l 0].';
p3_L = p4_L+ [C*cos(alfa2_l+beta2_l-alpha8),C*sin(alfa2_l+beta2_l-alpha8),0].';
% p4_pr = p1+([D*cos(alpha8),D*sin(alpha8),0]).';
% p4_prpr = [d*cos(alpha9)+D*cos(alpha8) d*sin(alpha9)+D*sin(alpha8) 0].';


% position of the wing
% p_wing_L = p4_L+[W*cos(alfa2_l+beta2_l-alpha8),W*sin(alfa2_l+beta2_l-alpha8),0].';


% output q2, which is the angle from horizontal to link34                
q2_L = pi-(alfa2_l+beta2_l-alpha8);

% wing articulation for the left side

% gamma_wing = atan2(delta_wingY,delta_wingX);
% p1_temp = p4+[0;0;0];
% p4_temp = p1_temp+D1*[cos(gamma_wing);sin(gamma_wing);0];
% p2_temp = p1_temp+A1*[cos(q2); sin(q2);0];
% theta_wing = q2-gamma_wing;
% E1 = sqrt(A1^2 + D1^2 - 2*A1*D1*cos(theta_wing));
% alfa3 = asin(A1*sin(theta_wing)/E1);
% beta3 = acos((E1^2 + C1^2 - B^2)/(2*E1*C1));
% p3_temp = p4_temp + C1*[-cos(alfa3+beta3-gamma_wing);sin(alfa3+beta3-gamma_wing);0 ];
% 
% 
% gamma_wing2 = acos((B1^2+E1^2-C1^2)/(2*B1*E1));
% gamma_wing3 = acos((A1^2+E1^2-B1^2)/(2*A1*E1)); 
% gamma_wing4 = alfa3-gamma_wing;
% gamma_wing5 = gamma_wing4-gamma_wing2;
% p_wingTip = 3*B1*[cos(gamma_wing5);-sin(gamma_wing5);0]
% 
% 
% 
% q3 = -(pi-gamma_wing2-gamma_wing3);







end















clear all;



params;
% this is the angle that the cam arm starts to touch the bat frame
temp1 = l1/lr;
alpha_rotation = asin(temp1);
temp2 = (lr-l1)/lh;
gamma2 = acos(temp2);

n = 30;
q1 = linspace(alpha_rotation,gamma2,n);
x0 = -10*pi/180;
% q1 = linspace(gamma1,gamma2,n);
% calculate the position of each point in the flapping mechanism
for i = 1:n
    
    [   pA(i,:),pE(i,:),p1(i,:),p2(i,:),p3(i,:),p4(i,:),...
        pb_R(i,:),pd_R(i,:),pc_R(i,:),pe_R(i,:),p_wing(i,:),...
        pE_L(i,:),p1_L(i,:),p2_L(i,:),p3_L(i,:),p4_L(i,:),...
        pb_L(i,:),pd_L(i,:),pc_L(i,:),pe_L(i,:),p_wing_L(i,:),...
        q2(i,:),q2_L(i,:)...
        pw2(i,:),pW2(i,:),q3(i,:),pE_wire_L(i,:)...
        gamma2]= func_fwdFlapping(q1(i),alpha_rotation,x0);
        x0 = gamma2;
   
    
end
%%

% plot
figure;

filename = 'sim_bat.gif';
for i = 1:n
    
    ground = plot([pA(i,1),pA(i,1)],[pA(i,2),pA(i,2)],'o','MarkerFaceColor','k');
    hold on
    plot([pE(i,1),pE(i,1)],[pE(i,2),pE(i,2)],'o');
    plot([p1(i,1),p1(i,1)],[p1(i,2),p1(i,2)],'o','MarkerFaceColor','k');
    plot([p2(i,1),p2(i,1)],[p2(i,2),p2(i,2)],'o');
    plot([p3(i,1),p3(i,1)],[p3(i,2),p3(i,2)],'o');
    plot([pd_R(i,1),pd_R(i,1)],[pd_R(i,2),pd_R(i,2)],'o');
    plot([p4(i,1),p4(i,1)],[p4(i,2),p4(i,2)],'o','MarkerFaceColor','k');
    
    plot([pE_L(i,1),pE_L(i,1)],[pE_L(i,2),pE_L(i,2)],'o');
    plot([p1_L(i,1),p1_L(i,1)],[p1_L(i,2),p1_L(i,2)],'o','MarkerFaceColor','k');
    plot([p2_L(i,1),p2_L(i,1)],[p2_L(i,2),p2_L(i,2)],'o');
    plot([p3_L(i,1),p3_L(i,1)],[p3_L(i,2),p3_L(i,2)],'o');
    plot([pd_L(i,1),pd_L(i,1)],[pd_L(i,2),pd_L(i,2)],'o');
    plot([p4_L(i,1),p4_L(i,1)],[p4_L(i,2),p4_L(i,2)],'o','MarkerFaceColor','k');
    
    plot([pA(i,1),pE(i,1)],[pA(i,2),pE(i,2)],'--');
    plot([p2(i,1),pE(i,1)],[p2(i,2),pE(i,2)]);
    plot([p2(i,1),p3(i,1)],[p2(i,2),p3(i,2)]);
    plot([p3(i,1),p4(i,1)],[p3(i,2),p4(i,2)]);
    plot([p2(i,1),p1(i,1)],[p2(i,2),p1(i,2)]);
%     plot([pE(i,1),pE_wire(i,1)],[pE(i,2),pE_wire(i,2)]);
    
    plot([pA(i,1),pE_L(i,1)],[pA(i,2),pE_L(i,2)],'--');
    plot([p2_L(i,1),pE_L(i,1)],[p2_L(i,2),pE_L(i,2)]);
    plot([p2_L(i,1),p3_L(i,1)],[p2_L(i,2),p3_L(i,2)]);
    plot([p3_L(i,1),p4_L(i,1)],[p3_L(i,2),p4_L(i,2)]);
    plot([p2_L(i,1),p1_L(i,1)],[p2_L(i,2),p1_L(i,2)]);
    plot([pE_wire_L(i,1),pw2(i,1)],[pE_wire_L(i,2),pw2(i,2)]);
    
    % plot the wing
    wing = plot([p4(i,1),p_wing(i,1)],[p4(i,2),p_wing(i,2)],'linewidth',3);
    plot([pw2(i,1),p_wing(i,1)],[pw2(i,2),p_wing(i,2)]);
    %     plot([pw2(i,1),pg(i,1)],[pw2(i,2),pg(i,2)]);
        plot([pE_wire_L(i,1),pE_L(i,1)],[pE_wire_L(i,2),pE_L(i,2)]);
    plot([pw2(i,1),pW2(i,1)],[pw2(i,2),pW2(i,2)]);
%     plot([pE_L(i,1),pw2(i,1)],[pE_L(i,2),pw2(i,2)]);
    
    plot([p4_L(i,1),p_wing_L(i,1)],[p4_L(i,2),p_wing_L(i,2)],'linewidth',3);
%     plot([pw2_L(i,1),p_wing_L(i,1)],[pw2_L(i,2),p_wing_L(i,2)]);
%     plot([pw2_L(i,1),pg_L(i,1)],[pw2_L(i,2),pg_L(i,2)]);
%     plot([pE_wire(i,1),pg_L(i,1)],[pE_wire(i,2),pg_L(i,2)]);
%     plot([pw2_L(i,1),pW2_L(i,1)],[pw2_L(i,2),pW2_L(i,2)]);
    
    
    
    % plot the frame
    cam = plot([pd_L(i,1),pd_R(i,1)],[pd_L(i,2),pd_R(i,2)],'linewidth',2);
    motor = plot([pb_R(i,1),pb_R(i,1)],[pb_R(i,2),pb_R(i,2)],'o','MarkerFaceColor','b');
    plot([pA(i,1),pc_R(i,1)],[pA(i,2),pc_R(i,2)]);
    plot([pc_R(i,1),pe_R(i,1)],[pc_R(i,2),pe_R(i,2)]);
    plot([pA(i,1),pc_L(i,1)],[pA(i,2),pc_L(i,2)]);
    plot([pc_L(i,1),pe_L(i,1)],[pc_L(i,2),pe_L(i,2)]);
    
    hold off
    xlabel('x');
    ylabel('y');
    axis equal
    xlim([-13 13]);
    ylim([-8 8]);
%     xlim([-2 15]);
%     ylim([-8 8]);
    
    title('bio-bat');
    xlabel('x');
    ylabel('y');
    legend([ground cam motor wing],'Ground','Cam Arm','motor','wing');
    
    
    
    box on
    frame = getframe(gcf);
    im = frame2im(frame);
    [imgin, cm] = rgb2ind(im, 256);
    
    if i == 1
        imwrite(imgin,cm,filename,'gif', 'Loopcount', inf);
    else
        imwrite(imgin,cm,filename,'gif', 'WriteMode','append');
    end
    
end

%%


figure;
plot(q1*180/pi);
hold on
plot(q2*180/pi);
plot(q2_L*180/pi);
plot(q3*180/pi);
hold off
xlabel('n[time step]');
ylabel('angle[degree]');
xlim([-2 n+2]);
ylim([-180 180]);
legend('q1-input','q2','q2_L','q_3');
title('joint trajectory');


%%
figure;
subplot(1,2,1);
pointE = plot(pE(:,1),pE(:,2),'linewidth',2,'color','r');
hold on
pointw_ing = plot(p_wing(:,1),p_wing(:,2),'linewidth',2,'color','k');
point2 = plot(p2(:,1),p2(:,2),'linewidth',2,'color','b');
pointwingTip = plot(pW2(:,1),pW2(:,2),'linewidth',2,'color','g');
pivot = plot([pA(1,1),pA(1,1)],[pA(1,2),pA(1,2)],'o');
cam = plot([pb_R(1,1),pb_R(1,1)],[pb_R(1,2),pb_R(1,2)],'o');
hold off
legend([pointE point2 pointw_ing pointwingTip pivot cam],'pointE','point2','elbow','wing tip','pivot','cam')
title('position trajectory');
xlabel('x[m]')
ylabel('y[m]')




subplot(1,2,2);
angle_E = atan2(pE(:,2),pE(:,1));
angle_2 = atan2(p2(:,2),p2(:,1));
angle_wingtip = atan2(pW2(:,2),pW2(:,1));
angle_elbow = atan2(p_wing(:,2),p_wing(:,1));

angleq1 = plot(q1*180/pi,'.-','color','k');
hold on
angle_E1 = plot(angle_E*180/pi,'color','r');
angle_21 = plot(angle_2*180/pi,'color','b');
angle_wingtip1 = plot(angle_wingtip*180/pi,'color','g');
angle_elbow1 = plot(angle_elbow*180/pi,'color','k');
hold off
incremental = pi/n*180/pi;

xlabel(sprintf('[angle]%2.2f deg incremental\n',incremental));
ylabel('angle[deg]')
legend([angleq1 angle_E1 angle_21 angle_wingtip1 angle_elbow1],'input-q_1','pointE','point2','elbow','wing tip');
title('orientation with respect to pivot from horizontal line')



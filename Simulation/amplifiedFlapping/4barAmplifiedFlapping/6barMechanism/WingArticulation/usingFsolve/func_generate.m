syms a  b  A  y

eqn =  a*sin(A)+b*cos(A) == y;
ans = solve(eqn,A);
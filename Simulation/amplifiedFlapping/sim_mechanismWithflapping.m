% Kinematics simulation of bio-bat

clc;
clear all;
close all;
% q is the motor angle
params;


temp1 = l1_R/lr;
gamma1 = asin(temp1);

n = 30;
q1 = linspace(0,pi,n);


for i = 1:n
[pa_R(:,i),pb_R(:,i),pc_R(:,i),pd_frame_R(:,i),pd_R(:,i),pe_R(:,i),pf_R(:,i),pg_R(:,i),q2(:,i)] = func_fwdFlapping(q1(i));
end





figure;
filename = 'sim_batflapping.gif';
for i = 1:n
    
% plot 3d of the robot
% right side


plot([pa_R(1,i),pa_R(1,i)],[pa_R(2,i),pa_R(2,i)],'o');
hold on
plot([pd_R(1,i),pd_R(1,i)],[pd_R(2,i),pd_R(2,i)],'o');
plot([pb_R(1,i),pb_R(1,i)],[pb_R(2,i),pb_R(2,i)],'o');
plot([pf_R(1,i),pf_R(1,i)],[pf_R(2,i),pf_R(2,i)],'o');
%  plot([pf_R(1),pf_R(1)],[pf_R(2),pf_R(2)],'o');
 
plot([pc_R(1,i),pa_R(1,i)],[pc_R(2,i),pa_R(2,i)]);
plot([pc_R(1,i),pe_R(1,i)],[pc_R(2,i),pe_R(2,i)]);
plot([pe_R(1,i),pf_R(1,i)],[pe_R(2,i),pf_R(2,i)]);
plot([pg_R(1,i),pf_R(1,i)],[pg_R(2,i),pf_R(2,i)],'linewidth',2);

cam_R = plot([pb_R(1,i),pd_R(1,i)],[pb_R(2,i),pd_R(2,i)],'linewidth',3);


% plot([pe(1,i),pd(1,i)],[pe(2,i),pd(2,i)]);

hold off
legend([cam_R],'cam right arm');
axis equal
xlim([-0.5 2]);
ylim([-0.5 1.5]);


title('bio-bat');
xlabel('x');
ylabel('y');



box on


% set the window size
% set(gcf,'position',[100,60,1300,870]);

% pause(0.2);
       frame = getframe(gcf);
    im = frame2im(frame);
    [imgin, cm] = rgb2ind(im, 256);
    
    if i == 1
        imwrite(imgin,cm,filename,'gif', 'Loopcount', inf);
    else
        imwrite(imgin,cm,filename,'gif', 'WriteMode','append');
    end
    

end

%%
figure;
ax1 = plot(q1*180/pi);
hold on
ax2 = plot(q2*180/pi);
hold off
xlabel('n[time step]');
ylabel('q2[degree]')
title('joint trajectory');
legend([ax1 ax2],'q_1','q_2')



